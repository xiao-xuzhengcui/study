package com.xiaoli.swaggerdemo.controller;

import com.xiaoli.swaggerdemo.pojo.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @ApiOperation("测试接口")//添加的注释
    @GetMapping(value = "/test")
    public String test(@ApiParam("用户名")String username){//@ApiParam("用户名")参数注解
        return "test";
    }

    @GetMapping(value = "/user")
    public User usertest(){//只要注解中有返回实体类，swagger注解中就会出现实体类的model
        User user = new User( "肖立",10);
        return user;
    }
}
