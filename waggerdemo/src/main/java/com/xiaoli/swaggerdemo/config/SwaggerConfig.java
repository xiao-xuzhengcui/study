package com.xiaoli.swaggerdemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {


    @Bean
    public Docket docket1(){
        return  new Docket(DocumentationType.SWAGGER_2)
                .groupName("xiaoli1");//分组
    }

    @Bean
    public Docket docket2(){
        return  new Docket(DocumentationType.SWAGGER_2)
                .groupName("xiaoli2");//分组
    }

    /**
     * 配置swagger的docket的实例
     * @return
     */
    @Bean
    public Docket docket(Environment environment){

        //添加head参数start
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<Parameter>();
        tokenPar.name("x-access-token").description("令牌").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        pars.add(tokenPar.build());

        //设置要显示的swagger环境
        Profiles profiles=Profiles.of("dev");
        //判断是否处于要显示的swagger注解的环境中
        boolean flag = environment.acceptsProfiles(profiles);

        return  new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(flag)//true开启swagger注解，false关闭swagger注解
                .groupName("xiaoli")//分组
                .select()
                //basePackage  指定扫描包
                //any() 扫描全部的包
                //none(): 不扫描
                //withClassAnnotation()扫描类上的注解，参数是一个注解的反射对象
                //withMethodAnnotation()扫描方法上的注解
                .apis(RequestHandlerSelectors.basePackage("com.xiaoli.swaggerdemo.controller"))//指定扫描的包
                //.paths(PathSelectors.ant("/xiaoli"))//过滤什么路径
                .build();
                //.globalOperationParameters(pars);//设置头信息
    }

    private ApiInfo apiInfo(){
        Contact contact=new Contact("肖立","http://localhost:8080/test","1258512794@qq.com");

        return new ApiInfo(
                "swagger 测试案例",
                "即使再小的帆也能远航",
                "1.0",
                "http://localhost:8080/test",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }
}
